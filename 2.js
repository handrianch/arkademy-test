'use strict';

const is_username = (name) => {
  let pattern = /^[a-z.]{8}$/
  let regex = pattern.exec(name);

  if(regex) {
    return true;
  }

  return false;
}

const is_email_valid = (email) => {
  let pattern = /^[a-zA-Z0-9.]{4,}@\w+\.(com|org)$/;

  if(pattern.exec(email)) {
    return true;
  }

  return false;
}

console.log(is_username('username')); // true
console.log(is_username('usernames')); // false
console.log(is_email_valid('kamu@aku.com')); // true
console.log(is_email_valid('aku@kamu.com')); // false