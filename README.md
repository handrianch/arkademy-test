

# Jawaban Soal Test Arkademy


##### Sistem Operasi yang digunakan
  * Linux Mint 18.2 x64
##### Software yang digunakan
  * Apache 2.4.38
  * MariaDB 10.1.38
  * PHP 7.3
  * Node JS v11.12.0
  * Sublime Text 3
  * Framework Codeigniter
---
##### Jawaban soal 1 - 5
untuk jawaban no 1 - 5 menggunakan bahasa javascript yang dijalankan dengan nodejs. untuk menjalankan nya cukup ketik node nama_file

<dl>
  <dt>Apa Itu REST API ?</dt>
  <dd>REST API adalah <em>Aplication Programming Interface<em> yang menggunakan <em>HTTP Request<em> GET, PUT, POST atau DELETE dalam pengkasesan nya.</dd>

  <dt>Kegunaan JSON pada REST API</dt>
  <dd>JSON (<em>Javascript Object Notation<em>) digunakan pada REST API untuk pertukaran penyimpanan data</dd>
</dl>


---

##### Jawaban soal no 6
Untuk jawaban pada soal ini saya menggunakan framework codeignter dan database mysql. sebelum dijalankan sesuiakan configurasi codeiginiter nya terlebih dahulu dengan configurasi server yang akan menjalankan

###### Demo aplikasi
![alt text](https://github.com/handrianch/arkademy-test/blob/master/6/ss.png?raw=true "Logo Title Text 1")

---