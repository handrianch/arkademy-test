'use strict';

const printBiodata = () => {
  let biodata = {
    name : "Candra Handrian",
    address : "Puri citra Blok G.6 No.4 Pipitan Serang Banten",
    hobbies : ['Membaca buku yang berisi tentang ilmu keagamaan islam dan tentang programming', 'bersepeda', 'ngoding' ],
    is_married : false,
    school : {
      highSchool : "SMK Insan Aqilah",
      university: "Universitas Bina Sarana Informatika"
    },
    skills : [
      {name: "html", score: 80},
      {name: "css", score: 60},
      {name: "javascript", score: 70},
      {name: "php", score: 80},
      {name: "mysql", score: 75},
      {name: "Laravel", score: 70},
      {name: "Codeigniter", score: 70}
    ]
  }

  return JSON.stringify(biodata);
}

console.log(printBiodata());