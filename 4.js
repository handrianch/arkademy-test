'use strict'

const printFlagsJollyRoger = () => {

  let strArr = ['P', 'R', 'O', 'G', 'R', 'A', 'M', 'M', 'E', 'R'];
  let flag = [];

  for( let i = 0; i < strArr.length; i++) {
    flag[i] = [];

    for( let j = 0; j < strArr.length; j++) {
      if( i === j ) {
        flag[i].push(strArr[i]);
      } else if((strArr.length - i) === j + 1 ) {
        flag[i].push(strArr[i]);
      } else {
        flag[i].push("=");
      }
    }
  }

  flag.forEach(item => {
    let text = '';
    item.forEach(val => text += val + " ");
    console.log(text);
    text = '';
  })
}


printFlagsJollyRoger()