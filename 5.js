'use strict';

const sortMaxVal = (arr) => {
  let finalData = [];

  arr.forEach(item => {
    let max = '';
    for( let i = 0; i < item.length; i++ ) {
      if(item[i] > max) {
        max = item[i];
      }
    }
    finalData.push(max);
  });

  return finalData;
}

let arr = [
  ['a', 'c', 'f'],
  ['x', 'a', 'n', 'z'],
  ['a', 'h', 'i', 'v']
];

console.log(sortMaxVal(arr));