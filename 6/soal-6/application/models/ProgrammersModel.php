<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProgrammersModel extends CI_Model
{
	public function store(array $data, string $table)
	{
		$this->db->insert($table, $data);
	}

	public function get($table)
	{
		return $this->db->get($table)->result();
	}

	public function getAllDataWithRelation(string $table, string $relation)
	{
		return $this->db->select('*')
										->from($table)
										->join($relation, "{$table}.id = {$relation}.user_id")
										->get()
										->result();
	}
}
