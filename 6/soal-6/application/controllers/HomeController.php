<?php
defined('BASEPATH') or exit("No direct script access allowed");

class HomeController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ProgrammersModel', 'Model');
	}

	public function index()
	{
		$users = $this->Model->get('users');
		$skills = $this->Model->getAllDataWithRelation('users', 'skills');

		$this->load->view('header');
		$this->load->view('home', compact('users', 'skills'));
		$this->load->view('footer');
	}

	public function addProgrammer()
	{
		$data = [
			'name' => $this->input->post('programmer', true)
		];

		$this->load->library('form_validation');
		$this->form_validation->set_rules('programmer', 'Nama Programmer', 'required');
		$this->form_validation->set_message('required', '{field} Harus diisi');

		if(!($this->form_validation->run())) {
			$this->session->set_flashdata('programmer-name', form_error('programmer'));
			return redirect('/');
		}

		$this->Model->store($data, 'users');

		return redirect('/');
	}

	public function addSkill($id)
	{
		$data = [
			'name' => $this->input->post('skill', true),
			'user_id' => $id
		];

		$this->Model->store($data, 'skills');

		return redirect('/');
	}
}
