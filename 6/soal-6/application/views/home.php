
<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-8 offset-md-2 col-sm-12">
			<div class="card">
				<div class="card-header">
					<p class="display-5 text-center">Data Programmer</p>
				</div>
				<div class="card-body">
					<div class="row mb-5">
						<div class="col">
							<form id="addProgrammer" method="POST" action="<?= base_url('add-programmer') ?>">
								<div class="input-group">
		  						<input type="text" class="form-control
		  						<?= !empty($this->session->flashdata('programmer-name')) ? "is-invalid" : "" ?>"
		  						name="programmer" placeholder="Tambah Programmer Baru">
									<div class="input-group-append">
		    							<button class="btn btn-primary" type="submit">Tambah</button>
		  						</div>
		  						<div class="invalid-feedback">
					        	<?= $this->session->flashdata('programmer-name') ?>
					      	</div>
								</div>
							</form>
						</div>
					</div>

					<?php foreach($users as $user) : ?>
						<div class="row mb-3">
							<table class="table table-bordered mx-3">
								<tr>
									<td class="h5"><?= $user->name ?></td>
									<td rowspan="2" class="align-middle">
										<form action="<?= base_url("add-skills/{$user->id}") ?>" method="POST">
											<div class="input-group">
					  						<input type="text" class="form-control" placeholder="Tambah Skills" name="skill">
												<div class="input-group-append">
					    							<button class="btn btn-primary" type="submit">Tambah</button>
					  						</div>
											</div>
										</form>
									</td>
								</tr>
								<tr>
									<td>
										<?php foreach($skills as $skill) : ?>
											<?php if($user->id === $skill->user_id) : ?>
												<?= $skill->name . ", " ?>
											<?php endif; ?>
										<?php endforeach; ?>
									</td>
								</tr>
							</table>
						</div>
					<?php endforeach; ?>


				</div>
		</div>
	</div>
</div>
