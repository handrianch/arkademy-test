'use strict';

const cetak = (total) => {
  let data = [];

  for( let i = 0; i < total; i++ ) {
    let strRandom = makeRandomString(32);
    data.push(strRandom);
  }


  for( let i = 0; i < data.length; i++ ) {
    if(data[i] === data[i+1]) {
      // generate random string lagi
      data[i+1] = makeRandomString(32);
      // set i to zero again
      i = 0;
    }
  }

  data.forEach((item) => console.log(item));
}



const makeRandomString = (length) => {
    let strDict = "abcdefghijklmnopqrstuvwxyz1234567890".split("");
    let stringRandom = '';

    for( let i = 0; i < length; i++ ) {
        let randomNumber = Math.floor(Math.random() * strDict.length);
        stringRandom += strDict[randomNumber];
    }

    return stringRandom;
}


cetak(3);